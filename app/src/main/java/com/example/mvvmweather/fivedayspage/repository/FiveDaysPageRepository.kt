package com.example.mvvmweather.fivedayspage.repository

import com.example.mvvmweather.fivedayspage.model.DetailWeatherData

interface FiveDaysPageRepository {
    suspend fun getFiveDaysWeatherData(
        params: FiveDaysPageParams
    ): DetailWeatherData?
}
