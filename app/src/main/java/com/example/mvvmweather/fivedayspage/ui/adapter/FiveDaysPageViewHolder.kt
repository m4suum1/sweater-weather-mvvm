package com.example.mvvmweather.fivedayspage.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmweather.fivedayspage.model.FiveDaysWeather
import com.example.mvvmweather.utils.extensions.dateFormat
import com.example.mvvmweather.utils.extensions.getDrawableIcon
import mvvmproject.databinding.ItemWeatherBinding

class FiveDaysPageViewHolder(
    private val binding: ItemWeatherBinding,
    private val clickOnItem: (FiveDaysWeather) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    constructor(
        parent: ViewGroup,
        onClickItem: (FiveDaysWeather) -> Unit
    ) : this(
        ItemWeatherBinding.inflate(LayoutInflater.from(parent.context), parent, false),
        onClickItem
    )

    private lateinit var description: String
    private lateinit var temperature: String
    private lateinit var date: String
    private lateinit var item: FiveDaysWeather

    fun getData(item: FiveDaysWeather) {
        description = item.description
        temperature = item.temp
        date = dateFormat(item.dtTxt)
        this.item = item
    }

    fun onBind() {
        with(binding) {
            imageViewIconItem.setImageResource(getDrawableIcon(description))
            textViewTemperature.text = temperature
            textViewDate.text = date
        }
        itemView.setOnClickListener {
            clickOnItem(item)
        }
    }
}
