package com.example.mvvmweather.fivedayspage.api

import com.example.mvvmweather.fivedayspage.api.model.DetailWeatherDataResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface FiveDaysPageWeatherApi {
    @GET("data/2.5/forecast")
    suspend fun getFiveDaysWeatherData(
        @Query("q") cityName: String,
        @Query("appid") appidKey: String,
        @Query("units") metric: String
    ): DetailWeatherDataResponse
}
