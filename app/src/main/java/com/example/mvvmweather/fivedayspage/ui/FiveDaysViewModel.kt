package com.example.mvvmweather.fivedayspage.ui

import com.example.mvvmweather.common.mvvm.BaseViewModel
import com.example.mvvmweather.fivedayspage.interactor.FiveDaysPageInteractor
import com.example.mvvmweather.fivedayspage.model.DetailWeatherData
import com.example.mvvmweather.fivedayspage.repository.FiveDaysPageParams
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber

class FiveDaysViewModel(
    private val interactor: FiveDaysPageInteractor
) : BaseViewModel() {

    //    private val _weatherData = MutableSharedFlow<DetailWeatherData>()
//    val weatherData: SharedFlow<DetailWeatherData>
    private val _weatherData = MutableSharedFlow<DetailWeatherData>().apply {
        @OptIn(ExperimentalCoroutinesApi::class)
        resetReplayCache()
    }
    val weatherData: SharedFlow<DetailWeatherData>
        //        get() = _weatherData
        get() = _weatherData.asSharedFlow()

    private val _loading = MutableStateFlow(false)

    val loading: StateFlow<Boolean>
        //        get() = _loading
        get() = _loading.asStateFlow()

    fun getWeatherData(params: FiveDaysPageParams) {
        launch {
            try {
//                _loading.emit(true)
                _loading.tryEmit(true)
                val weather = interactor.getWeatherData(params)
                _weatherData.emit(weather)
            } catch (t: Throwable) {
                Timber.e(t.message)
            } catch (e: CancellationException) {
                Timber.e(e.message)
            } finally {
//                _loading.emit(false)
                _loading.tryEmit(false)
            }
        }
    }
}
