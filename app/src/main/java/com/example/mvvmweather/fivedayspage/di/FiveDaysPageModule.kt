package com.example.mvvmweather.fivedayspage.di

import com.example.mvvmweather.common.di.InjectionModule
import com.example.mvvmweather.fivedayspage.api.FiveDaysPageWeatherApi
import com.example.mvvmweather.fivedayspage.repository.FiveDaysPageRemoteRepository
import com.example.mvvmweather.fivedayspage.repository.FiveDaysPageRepository
import org.koin.core.module.dsl.factoryOf
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit
import com.example.mvvmweather.fivedayspage.interactor.FiveDaysPageInteractor
import com.example.mvvmweather.fivedayspage.ui.FiveDaysViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.singleOf

object FiveDaysModule : InjectionModule {
    override fun create() = module {
        single { get<Retrofit>().create(FiveDaysPageWeatherApi::class.java) }
        single<FiveDaysPageRepository> { FiveDaysPageRemoteRepository(get()) }
        singleOf(::FiveDaysPageRemoteRepository) bind FiveDaysPageRepository::class
        factoryOf(::FiveDaysPageInteractor)
        viewModelOf(::FiveDaysViewModel)
    }
}
