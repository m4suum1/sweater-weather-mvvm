package com.example.mvvmweather.fivedayspage.interactor

import com.example.mvvmweather.fivedayspage.repository.FiveDaysPageRemoteRepository
import com.example.mvvmweather.fivedayspage.repository.FiveDaysPageParams

class FiveDaysPageInteractor(private val remoteRepository: FiveDaysPageRemoteRepository) {
    suspend fun getWeatherData(params: FiveDaysPageParams) =
        remoteRepository.getFiveDaysWeatherData(params = params)
}
