package com.example.mvvmweather.fivedayspage.api.model

import com.google.gson.annotations.SerializedName
import com.example.mvvmweather.mainpage.api.model.CloudsResponse
import com.example.mvvmweather.mainpage.api.model.MainResponse
import com.example.mvvmweather.mainpage.api.model.WeatherResponse
import com.example.mvvmweather.mainpage.api.model.WindResponse

data class FiveDaysWeatherResponse(
    @SerializedName("dt")
    val dt: Long,
    @SerializedName("main")
    val main: MainResponse,
    @SerializedName("weather")
    val weather: List<WeatherResponse>,
    @SerializedName("clouds")
    val clouds: CloudsResponse,
    @SerializedName("wind")
    val wind: WindResponse,
    @SerializedName("visibility")
    val visibility: Long,
    @SerializedName("pop")
    val pop: Double,
    @SerializedName("rain")
    val rain: RainResponse? = null,
    @SerializedName("sys")
    val sys: SysFiveDaysResponse,
    @SerializedName("dt_txt")
    val dtTxt: String
)
