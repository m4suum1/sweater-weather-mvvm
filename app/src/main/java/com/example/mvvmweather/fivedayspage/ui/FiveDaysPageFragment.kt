package com.example.mvvmweather.fivedayspage.ui

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvvmweather.common.mvvm.BaseFragment
import com.example.mvvmweather.detailsofday.DetailsOfDayFragment
import com.example.mvvmweather.fivedayspage.model.FiveDaysWeather
import com.example.mvvmweather.fivedayspage.repository.FiveDaysPageParams
import com.example.mvvmweather.fivedayspage.ui.adapter.FiveDaysPageAdapter
import com.example.mvvmweather.utils.Arguments.CITY_NAME
import com.example.mvvmweather.utils.extensions.args
import com.example.mvvmweather.utils.extensions.popScreen
import com.example.mvvmweather.utils.extensions.replaceScreen
import com.example.mvvmweather.utils.extensions.viewbinding.viewBinding
import com.example.mvvmweather.utils.extensions.withArgs
import mvvmproject.R
import mvvmproject.databinding.FragmentFiveDaysWeatherBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class FiveDaysPageFragment : BaseFragment(R.layout.fragment_five_days_weather) {

    companion object {
        fun newInstance(cityName: String) = FiveDaysPageFragment()
                .withArgs(CITY_NAME to cityName)
    }

    private val binding: FragmentFiveDaysWeatherBinding by viewBinding()

    private val cityName: String? by args(CITY_NAME)
    private val viewModel: FiveDaysViewModel by viewModel()


    private val adapter: FiveDaysPageAdapter by lazy {
        FiveDaysPageAdapter { item ->
            replaceScreen(DetailsOfDayFragment.newInstance(item))
        }
    }

    private val layoutManager: LinearLayoutManager by lazy {
        LinearLayoutManager(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val appidKey = getString(R.string.open_weather_key)
        val celsiusUnits = getString(R.string.celsius_units)

        val params =
            cityName?.let {
                FiveDaysPageParams(
                    cityName = it,
                    appidKey = appidKey,
                    metric = celsiusUnits
                )
            }

        //потом добавить контракт между FiveDaysPageFragment и WeatherDetailedPageFragment, чтобы второе условие имело смысл
        if (params != null && binding.toolBar.title != cityName) viewModel.getWeatherData(params)



//        viewLifecycleOwner.lifecycleScope.launch {
//            viewModel.weatherData.collect { data ->
//                showData(data.list)
//            }
//        }

        observe(viewModel.weatherData) { detailWeatherData ->
            showData(detailWeatherData.list)
        }

        with(binding) {
            toolBar.title = cityName
            recyclerViewFiveDaysWeather.layoutManager = layoutManager
            recyclerViewFiveDaysWeather.setHasFixedSize(true)
            recyclerViewFiveDaysWeather.adapter = adapter
            adapter.onAttachedToRecyclerView(recyclerViewFiveDaysWeather)

            toolBar.setNavigationOnClickListener {
                popScreen()
            }
//            viewLifecycleOwner.lifecycleScope.launch {
//                viewModel.loading.collect { isLoading ->
//                    showLoading(isLoading)
//                }
//            }

            observe(viewModel.loading) { isLoading ->
                showLoading(isLoading)
            }
        }
    }

    private fun showData(data: List<FiveDaysWeather>) {
        adapter.setData(data)
    }

    private fun showLoading(isLoading: Boolean) {
        binding.progressFiveDays.isVisible = isLoading
    }
}
