package com.example.mvvmweather.fivedayspage.repository

import com.example.mvvmweather.fivedayspage.api.FiveDaysPageWeatherApi
import com.example.mvvmweather.fivedayspage.model.DetailWeatherData
import com.example.mvvmweather.fivedayspage.model.FiveDaysPageConverter

class FiveDaysPageRemoteRepository(val api: FiveDaysPageWeatherApi) : FiveDaysPageRepository {

    override suspend fun getFiveDaysWeatherData(
        params: FiveDaysPageParams
    ) = FiveDaysPageConverter.fromNetwork(
        api.getFiveDaysWeatherData(
            cityName = params.cityName,
            appidKey = params.appidKey,
            metric = params.metric
        )
    )
}
