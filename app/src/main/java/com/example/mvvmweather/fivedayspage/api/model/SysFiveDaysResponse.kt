package com.example.mvvmweather.fivedayspage.api.model

import com.google.gson.annotations.SerializedName

data class SysFiveDaysResponse(
    @SerializedName("pod")
    val pod: String?
)
