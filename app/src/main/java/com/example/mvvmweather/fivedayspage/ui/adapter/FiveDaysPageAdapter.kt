package com.example.mvvmweather.fivedayspage.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmweather.fivedayspage.model.FiveDaysWeather
import mvvmproject.R

class FiveDaysPageAdapter (
    private val clickOnItem:(FiveDaysWeather) -> Unit
        ) : RecyclerView.Adapter<FiveDaysPageViewHolder>() {

    private val data = mutableListOf<FiveDaysWeather>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FiveDaysPageViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.item_weather, parent, false)
        return FiveDaysPageViewHolder(parent, clickOnItem)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: FiveDaysPageViewHolder, position: Int) {
        val listItem = data[position]
        holder.getData(listItem)
        holder.onBind()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(items: List<FiveDaysWeather>) {
        data.clear()
        data.addAll(items)
        notifyDataSetChanged()
    }
}
