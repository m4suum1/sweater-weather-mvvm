package com.example.mvvmweather.fivedayspage.repository

data class FiveDaysPageParams(
    val cityName: String,
    val appidKey: String,
    val metric: String
)
