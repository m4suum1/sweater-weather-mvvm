package com.example.mvvmweather

import android.app.Application
import com.example.mvvmweather.common.di.NetworkModule
import com.example.mvvmweather.fivedayspage.di.FiveDaysModule
import com.example.mvvmweather.mainpage.di.MainPageModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.context.GlobalContext.stopKoin
import timber.log.Timber


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        setupTimber()
        setupKoin()
    }

    private fun setupTimber() {
        Timber.plant(Timber.DebugTree())
    }

    private fun setupKoin() {
        stopKoin()
        startKoin {
            androidContext(this@App)
            modules(
                listOf(
                    NetworkModule.create(),
                    MainPageModule.create(),
                    FiveDaysModule.create()
                )
            )
        }
    }
}
