package com.example.mvvmweather.mainpage.model

import com.example.mvvmweather.mainpage.api.model.MainResponse
import com.example.mvvmweather.mainpage.api.model.WeatherDataResponse
import com.example.mvvmweather.mainpage.api.model.WeatherResponse
import com.example.mvvmweather.utils.extensions.tempDescription

object MainPageConverter {
    fun fromNetwork(response: WeatherDataResponse): WeatherData {
        return WeatherData(
            temp = fromNetworkTemp(response.main),
            feelsLike = fromNetworkFeelsLike(response.main),
            tempMin = fromNetworkTempMin(response.main),
            tempMax = fromNetworkTempMax(response.main),
            pressure = fromNetworkPressure(response.main),
            humidity = fromNetworkHumidity(response.main),
            description = fromNetworkDescription(response.weather),
            name = response.name.replaceFirstChar { it.uppercase() }
        )
    }

    private fun fromNetworkTemp(response: MainResponse): String {
        return tempDescription(response.temp)
    }

    private fun fromNetworkFeelsLike(response: MainResponse): String {
        return tempDescription(response.feelsLike)
    }

    private fun fromNetworkTempMin(response: MainResponse): String {
        return tempDescription(response.tempMin)
    }

    private fun fromNetworkTempMax(response: MainResponse): String {
        return tempDescription(response.tempMax)
    }

    private fun fromNetworkPressure(response: MainResponse): String {
        return response.pressure.toString()
    }

    private fun fromNetworkHumidity(response: MainResponse): String {
        return response.humidity.toString()
    }

    private fun fromNetworkDescription(response: List<WeatherResponse>): String {
        return response[0].description.replaceFirstChar { it.uppercase() }
    }
}
