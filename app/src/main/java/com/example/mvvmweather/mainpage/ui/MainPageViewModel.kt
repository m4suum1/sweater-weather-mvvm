package com.example.mvvmweather.mainpage.ui

import com.example.mvvmweather.common.mvvm.BaseViewModel
import com.example.mvvmweather.mainpage.interactor.MainPageInteractor
import com.example.mvvmweather.mainpage.model.WeatherData
import com.example.mvvmweather.mainpage.repository.MainPageParams
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber

class MainPageViewModel(
    private val interactor: MainPageInteractor
) : BaseViewModel() {

    private val _weatherFlow = MutableSharedFlow<WeatherData>().apply {
    @OptIn(ExperimentalCoroutinesApi::class)
        resetReplayCache()
    }
    val weatherFlow: SharedFlow<WeatherData>
//        get() = _weatherData
        get() = _weatherFlow.asSharedFlow()

    private val _loading = MutableStateFlow(false)
    val loading: StateFlow<Boolean>
//        get() = _loading
        get() = _loading.asStateFlow()

    fun getWeatherData(params: MainPageParams) {
        launch {
            try {
//                _loading.emit(true)
                _loading.tryEmit(true)
                val weatherData = interactor.getWeatherData(params)
                _weatherFlow.emit(weatherData)
            } catch (t: Throwable) {
                Timber.e(t.message)
            } catch (e: CancellationException) {
                Timber.e(e.message)
            } finally {
//                _loading.emit(false)
                _loading.tryEmit(false)
            }
        }
    }
}
