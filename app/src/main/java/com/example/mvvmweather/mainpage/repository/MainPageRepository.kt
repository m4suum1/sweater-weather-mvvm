package com.example.mvvmweather.mainpage.repository

import com.example.mvvmweather.mainpage.model.WeatherData

interface MainPageRepository {
    suspend fun getWeatherData(params: MainPageParams): WeatherData?
}
