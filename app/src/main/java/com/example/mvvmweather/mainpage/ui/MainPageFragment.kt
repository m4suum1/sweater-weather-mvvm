package com.example.mvvmweather.mainpage.ui

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.leanback.widget.Visibility
import com.example.mvvmweather.common.mvvm.BaseFragment
import com.example.mvvmweather.detailpage.WeatherDetailedPageFragment
import com.example.mvvmweather.mainpage.model.WeatherData
import com.example.mvvmweather.mainpage.repository.MainPageParams
import com.example.mvvmweather.utils.extensions.getDrawableIcon
import com.example.mvvmweather.utils.extensions.replaceScreen
import com.example.mvvmweather.utils.extensions.viewbinding.viewBinding
import mvvmproject.R
import mvvmproject.databinding.FragmentWeatherMainPageBinding
import org.koin.android.ext.android.inject

private const val TEXT_MIN_SIZE = 3
private const val TEXT_MAX_SIZE = 20

class MainPageFragment : BaseFragment(R.layout.fragment_weather_main_page) {

    private val binding: FragmentWeatherMainPageBinding by viewBinding()
    private val viewModel: MainPageViewModel by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val appidKey = getString(R.string.open_weather_key)
        val celsiusUnits = getString(R.string.celsius_units)

        with(binding) {
            textInputEditTextEnterCity.doAfterTextChanged {
                with (textInputLayout) {
                    if (it?.length in TEXT_MIN_SIZE..TEXT_MAX_SIZE) {
                        isErrorEnabled = false
                        val cityName = it.toString()
                        val params = MainPageParams(
                            cityName = cityName,
                            appidKey = appidKey,
                            metric = celsiusUnits
                        )
                        viewModel.getWeatherData(params)

                        observe(viewModel.weatherFlow) { weatherData ->
                            showWeatherData(weatherData)
                            changeIcon(weatherData)

                            buttonDetailedInfo.setOnClickListener {
                                replaceScreen(WeatherDetailedPageFragment.newInstance(weatherData))
                            }
                        }
                        observe(viewModel.loading) { isLoading ->
                            showLoading(isLoading)
                        }

                    } else if (it?.length!! < TEXT_MIN_SIZE) {
                        error =
                            resources.getString(R.string.text_input_layout_error_min)
                        isErrorEnabled = true
                    } else {
                        error =
                            resources.getString(R.string.text_input_layout_error_max)
                        isErrorEnabled = true
                    }
                }
            }
        }
    }

    private fun showWeatherData(data: WeatherData) {
        with(binding) {
            changeWeatherDataVisibility(View.VISIBLE)
            textViewCityName.text = data.name
            textViewTemperature.text = data.temp
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.progress.isVisible = isLoading
    }

    private fun changeIcon(data: WeatherData) {
        binding.imageViewIcon.setImageResource(getDrawableIcon(data.description))
    }

    private fun changeWeatherDataVisibility(@Visibility visibility: Int) {
        with(binding) {
            textInputLayout.visibility = visibility
            textViewCityName.visibility = visibility
            textViewTemperature.visibility = visibility
            buttonDetailedInfo.visibility = visibility
        }
    }
}
