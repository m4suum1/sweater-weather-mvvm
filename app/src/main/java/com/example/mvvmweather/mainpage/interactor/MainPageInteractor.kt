package com.example.mvvmweather.mainpage.interactor

import com.example.mvvmweather.mainpage.model.WeatherData
import com.example.mvvmweather.mainpage.repository.MainPageParams
import com.example.mvvmweather.mainpage.repository.MainPageRemoteRepository

class MainPageInteractor(private val remoteRepository: MainPageRemoteRepository) {
    suspend fun getWeatherData(params: MainPageParams): WeatherData =
        remoteRepository.getWeatherData(params)
}
