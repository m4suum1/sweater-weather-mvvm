package com.example.mvvmweather.mainpage.di

import com.example.mvvmweather.common.di.InjectionModule
import com.example.mvvmweather.mainpage.api.MainPageApi
import com.example.mvvmweather.mainpage.interactor.MainPageInteractor
import com.example.mvvmweather.mainpage.repository.MainPageRemoteRepository
import com.example.mvvmweather.mainpage.repository.MainPageRepository
import com.example.mvvmweather.mainpage.ui.MainPageViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.factoryOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

object MainPageModule : InjectionModule {

    override fun create() = module {
        single { get<Retrofit>().create(MainPageApi::class.java) } //private val api: WeatherApi = RetrofitClient.getClient().create(WeatherApi::class.java)
        single<MainPageRepository> { MainPageRemoteRepository(get()) } //-> creating Interface implementation
        singleOf(::MainPageRemoteRepository) bind MainPageRepository::class //private val remoteRepository = MainPageRemoteRepository(api)
        factoryOf(::MainPageInteractor) //private val interactor = MainPageInteractor(remoteRepository)
        viewModelOf(::MainPageViewModel)
    }
}
