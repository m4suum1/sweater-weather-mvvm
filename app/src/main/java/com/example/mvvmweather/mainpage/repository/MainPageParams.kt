package com.example.mvvmweather.mainpage.repository

data class MainPageParams(
    val cityName: String,
    val appidKey: String,
    val metric: String
)
