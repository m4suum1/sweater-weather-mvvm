package com.example.mvvmweather.mainpage.repository

import com.example.mvvmweather.mainpage.api.MainPageApi
import com.example.mvvmweather.mainpage.model.MainPageConverter

class MainPageRemoteRepository(val api: MainPageApi) : MainPageRepository {
    override suspend fun getWeatherData(
        params: MainPageParams
    ) = MainPageConverter.fromNetwork(
        api.getWeatherData(
            cityName = params.cityName,
            appidKey = params.appidKey,
            metric = params.metric
        )
    )
}
