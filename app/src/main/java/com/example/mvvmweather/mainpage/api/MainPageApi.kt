package com.example.mvvmweather.mainpage.api

import com.example.mvvmweather.mainpage.api.model.WeatherDataResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MainPageApi {
    @GET("data/2.5/weather")
    suspend fun getWeatherData(
        @Query("q") cityName: String,
        @Query("appid") appidKey: String,
        @Query("units") metric: String
    ): WeatherDataResponse
}
