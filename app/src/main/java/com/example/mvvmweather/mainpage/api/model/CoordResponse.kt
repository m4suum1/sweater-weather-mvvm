package com.example.mvvmweather.mainpage.api.model

import com.google.gson.annotations.SerializedName

data class CoordResponse(
    @SerializedName("lon")
    var lon: Float?,
    @SerializedName("lat")
    var lat: Float?
)
