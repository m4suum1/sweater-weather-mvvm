package com.example.mvvmweather.root

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.mvvmweather.mainpage.ui.MainPageFragment
import com.example.mvvmweather.utils.extensions.popFeature
import mvvmproject.R
import mvvmproject.databinding.ActivityRootBinding

class RootActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRootBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRootBinding.inflate(layoutInflater)
        setContentView(binding.root)
        replace(MainPageFragment())
    }

    @Deprecated(
        "Deprecated in Java",
        ReplaceWith("popFeature()", "mvvmproject.utils.extensions.popFeature")
    )
    override fun onBackPressed() {
        popFeature()
    }

    private fun replace(
        fragment: Fragment,
        tag: String = fragment::class.java.name
    ) {
        val fragmentManager = this.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction
            .replace(R.id.container, fragment, tag)
            .addToBackStack(tag)
            .commit()
    }
}
