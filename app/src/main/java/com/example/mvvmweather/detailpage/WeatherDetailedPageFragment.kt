package com.example.mvvmweather.detailpage

import android.os.Bundle
import android.view.View
import com.example.mvvmweather.common.mvvm.BaseFragment
import com.example.mvvmweather.fivedayspage.ui.FiveDaysPageFragment
import com.example.mvvmweather.mainpage.model.WeatherData
import com.example.mvvmweather.utils.Arguments
import com.example.mvvmweather.utils.extensions.args
import com.example.mvvmweather.utils.extensions.popScreen
import com.example.mvvmweather.utils.extensions.replaceScreen
import com.example.mvvmweather.utils.extensions.viewbinding.viewBinding
import com.example.mvvmweather.utils.extensions.withArgs
import mvvmproject.R
import mvvmproject.databinding.FragmentWeatherDetailedPageBinding

class WeatherDetailedPageFragment : BaseFragment(R.layout.fragment_weather_detailed_page) {
    private val binding: FragmentWeatherDetailedPageBinding by viewBinding()

    companion object {
        fun newInstance(weatherData: WeatherData?) = WeatherDetailedPageFragment()
            .withArgs(Arguments.WEATHER_DATA to weatherData)
    }

    private val weatherData: WeatherData? by args(Arguments.WEATHER_DATA)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showDetailedWeather()

        with(binding) {
            toolBarDetailPageTitle.setNavigationOnClickListener {
                popScreen()
            }

            buttonFiveDaysInfo.setOnClickListener {
                val cityName = toolBarDetailPageTitle.title.toString()
                replaceScreen(FiveDaysPageFragment.newInstance(cityName))

//                val fragment = parentFragmentManager.findFragmentByTag(FiveDaysPageFragment()::class.java.name)
//                if (fragment?.arguments?.getString(CITY_NAME) != cityName)
//                replaceScreen(FiveDaysPageFragment.newInstance(cityName))
//                else replaceScreen(fragment) нарабочая фигня

            }
        }
    }

    private fun showDetailedWeather() {
        with(binding) {
            toolBarDetailPageTitle.title = weatherData?.name
            textViewFeelsLike.text = weatherData?.feelsLike
            textViewTempMin.text = weatherData?.tempMin
            textViewTempMax.text = weatherData?.tempMax
            textViewPressure.text = weatherData?.pressure
            textViewHumidity.text = weatherData?.humidity
        }
    }
}
